package com.jj.indexer.config;

import com.jj.indexer.model.Product;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.batch.item.database.builder.JpaPagingItemReaderBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManagerFactory;

@Slf4j
@Configuration
@AllArgsConstructor
public class JobConfiguration {
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final EntityManagerFactory entityManagerFactory;
    private final RestHighLevelClient restHighLevelClient;

    private static final String INDEX = "product";
    private static final String TYPE = "doc";


    @Bean
    public Job indexerJob() {
        return jobBuilderFactory.get("indexerJob")
                .start(indexerStep())
                .build();
    }

    @Bean
    @JobScope
    public Step indexerStep() {
        return stepBuilderFactory.get("indexerStep")
                .<Product, Product>chunk(1)
                .reader(itemReader())
                .writer(itemWriter())
                .build();
    }

    @Bean
    public JpaPagingItemReader<Product> itemReader() {
        log.info("product reader");
        return new JpaPagingItemReaderBuilder<Product>()
                .name("itemReader")
                .entityManagerFactory(entityManagerFactory)
                .queryString("SELECT p FROM Product p")
                .pageSize(5)
                .build();
    }

    private ItemWriter<Product> itemWriter() {
        final BulkRequest bulkRequest = new BulkRequest();
        return (products) -> {
            products.parallelStream()
                    .forEach((product) ->
                            bulkRequest.add(
                                    new IndexRequest()
                                            .index(INDEX)
                                            .type(TYPE)
                                            .id(product.getProductId())
                                            .source("test-field", "test-value")
                            )
                    );
            restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        };
    }

}
