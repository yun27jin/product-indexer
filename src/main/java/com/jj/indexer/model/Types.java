package com.jj.indexer.model;

/**
 * Ordinary 하지않은 EnumType 의 Static 을 위하여 반드시 필요한 개념을 구현한 인터페이스..
 * Enum 을 Ordinary 하게 그냥 구현하면 추후 추가 Enum 발생 시 또는 빈 값 생성 시 쓸데없는 값을 넣게 된다.
 * 아직 abstract class 등을 이용한 보일러 플레이트를 제거하는 방법은 모르겠음...
 *
 * @param <T> enum 으로 구현한 하위의 subClass 이다.
 * @author  teahyuk
 * @see java.lang.Enum
 */
interface Types<T extends Enum<T>> {
	int getTypeNumber();

	/**
	 *  Number 에 따른 Enum 타입을 반환하도록 한다.
	 *  해당 보일러 플레이트를 사용하면 쉬움.</br>
	 *  private static Map<Integer, T> valuesMap = Arrays.stream(T.values())</br>
	 *             .collect(Collectors.toMap(v->v.typeNumber, Function.identity()));</br></br>
	 *
	 *  return valuesMap.get(typeNumber);
	 *
	 * @param typeNumber Enum 의 기본이 되는 Type
	 * @see Enum#valueOf(Class, String)
	 * @return enum 으로 구현한 하위의 subClass 이다.
	 */
	T valueOf(int typeNumber);
}
