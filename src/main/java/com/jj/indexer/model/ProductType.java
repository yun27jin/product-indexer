package com.jj.indexer.model;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum  ProductType implements Types<ProductType>{

    일반(1),
    중고(2),
    단종(3);

    private int typeNumber;

    private static Map<Integer, ProductType> valuesMap = Arrays.stream(values())
            .collect(Collectors.toMap(v->v.typeNumber, Function.identity()));

    ProductType(int typeNumber) {
        this.typeNumber = typeNumber;
    }

    @Override
    public ProductType valueOf(int typeNumber) {
        return valuesMap.get(typeNumber);
    }

    @Override
    public int getTypeNumber() {
        return typeNumber;
    }
}
