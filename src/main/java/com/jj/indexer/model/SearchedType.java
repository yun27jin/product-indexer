package com.jj.indexer.model;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public enum SearchedType implements Types<SearchedType> {

    가격비교상품(1),
    가격비교비매칭일반상품(2),
    가격비교매칭일반상품(3);

    private int typeNumber;

    private static Map<Integer, SearchedType> valuesMap = Arrays.stream(SearchedType.values())
            .collect(Collectors.toMap(v -> v.typeNumber, Function.identity()));

    SearchedType(int typeNumber) {
        this.typeNumber = typeNumber;
    }

    @Override
    public SearchedType valueOf(int typeNumber) {
        return valuesMap.get(typeNumber);
    }

    @Override
    public int getTypeNumber() {
        return typeNumber;
    }

}