package com.jj.indexer.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Product {
    @Id
    private String productId;
    private String title;
    private LocalDateTime lastBuildDate;
    private Integer total;
    private Integer start;
    private Integer display;
    private String link;
    private String image;
    private Integer lPrice;
    private Integer hPrice;
    private String mallName;
//    private ProductType productType;
//    private SearchedType searchedType;
    private Integer productType;
    private Integer searchedType;
}