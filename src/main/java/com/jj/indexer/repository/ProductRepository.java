package com.jj.indexer.repository;

import com.jj.indexer.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, String> {
}
